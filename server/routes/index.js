var express = require('express');
var router = express.Router();
const { getWeatherForToday } = require('../api');

router.get('/api/data', async (req, res) => {
  const { data } = await getWeatherForToday();

  res.send(data);
});

module.exports = router;
