const testOptions = {
    "location": [50.450001, 30.523333], // kyiv
    "fields": ["weatherCode", "windSpeed"],
    "timesteps": ["1d"],
    "units": "metric",
};

module.exports = {
    testOptions
}
