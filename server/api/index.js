const axios = require('axios');
const { testOptions } = require('../utils/data');

const baseAxiosInstance = axios.create({
    baseURL: 'https://api.tomorrow.io/v4',
    responseType: "json",
    headers: {
        "apikey": process.env.TOMORROW_API_KEY
    },
});

const getDate = () => {
    const startTime = new Date();
    const endTime = new Date(startTime);
    endTime.setDate(startTime.getDate() + 7);
    startTime.toISOString();
    endTime.toISOString();

    return [startTime, endTime];
}

const getWeatherForToday = async () => {

   const [startTime, endTime] = getDate();

    const { data } = await baseAxiosInstance.post('/timelines', {
            ...testOptions,
            startTime,
            endTime,
    });

    return data;
};

module.exports = {
    testOptions,
    getWeatherForToday
}

