import { get } from 'lodash';

const getBiggestNumber = (array) => array.reduce((curr, acc) => Math.max(curr, acc), 0)

export const prepareData = (data) => {
    let dataForChart = [];

    const { timelines  } = data;

    const intervals = get(timelines, '[0].intervals', '');

    const biggestNum = intervals.map(dateInfo => dateInfo.values.windSpeed);

    for (let dateInfo of intervals){
        const { startTime, values: { weatherCode, windSpeed } } = dateInfo;
        dataForChart.push({
            startTime: new Date(startTime).toLocaleDateString(),
            weatherCode,
            windSpeed,
            maxNonHatFallSpeed: 5,
        });
    };

    return {
        dataForChart,
        biggestNum: getBiggestNumber(biggestNum),
    };
}
