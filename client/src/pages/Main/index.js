import React, {useEffect, useState} from 'react';
import {
    AreaChart,
    LabelList,
    YAxis,
    XAxis,
    CartesianGrid,
    Area,
} from 'recharts';
import { isEmpty } from "lodash";
import { APIMethods } from "../../api";
import { prepareData } from "../../helpers";
import styles from './styles.module.scss';

const Main = () => {

    const [weatherInfo, setWeatherInfo] = useState(null);

    useEffect(() => {
        (async () => {
            const data  = await APIMethods.getWeather();
            const finalized = prepareData(data);
            setWeatherInfo(finalized);
        })()
    }, []);

    return (
        <div className={styles.MainPageContainer}>
            <h3>Would the hat fall off this week?</h3>
            {
                !isEmpty(weatherInfo) && (
                    <AreaChart
                        width={730}
                        height={250}
                        data={weatherInfo.dataForChart}
                        className={styles.Chart}
                    >
                        <defs>
                            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                            </linearGradient>
                            <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
                                <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
                            </linearGradient>
                        </defs>
                        <LabelList dataKey='windSpeed'/>
                        <XAxis dataKey="startTime" />
                        <YAxis
                            dataKey='windSpeed'
                            interval="preserveStartEnd"
                            allowDecimals
                        />
                        <CartesianGrid strokeDasharray="3 3" />
                        <Area type="monotone" dataKey="windSpeed" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
                        <Area type="monotone" dataKey="maxNonHatFallSpeed" stroke="#82ca9d" fillOpacity={0.2} fill="url(#colorPv)" />
                    </AreaChart>
                )
            }
            <div className={styles.InfoContainer}>
                <p>
                    The Y-axis displays the wind speed for the day depicted on the X-axis. The transparent line shows if
                    the hat will fall off, as said by google, the necessary speed is about 5 km/h.
                </p>
            </div>
        </div>
    )
};

export default Main;
