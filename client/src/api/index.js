import axios from 'axios';

const urlOrigin = window.origin === 'localhost' ? `${window.location.origin}:5000` : window.location.origin;

const APIClient = axios.create({
    baseURL: urlOrigin,
    headers: {
        'Access-Control-Allow-Origin': '*',
    }
})

export const APIMethods = {
    getWeather: async () => {
        const { data } = await APIClient.get('/api/data');

        return data;
    } ,
};
